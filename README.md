# Adguard home rasberrypi



## Information

Bienvenue sur le Gitlab de jonathan, votre hub de filtrage AdGuard! 🛡️

ici, nous mettons à votre disposition une sélection complète des filtres AdGuard, offrant une protection et un filtrage de premier ordre pour rendre votre expérience en ligne plus sûre et plus fluide que jamais. 💻✨

Que vous cherchiez à bloquer les publicités intrusives, à vous protéger contre les sites malveillants, ou à personnaliser votre navigation, nous avons les filtres AdGuard dont vous avez besoin. 🚫🌐

Notre mission est de vous offrir un accès facile à ces filtres pour que vous puissiez profiter d'un Internet plus propre et plus sécurisé. Explorez notre collection de filtres AdGuard dès aujourd'hui et renforcez votre cybersécurité tout en améliorant votre expérience en ligne! 🌟🔒

Visitez notre lab maintenant et découvrez comment nos filtres AdGuard peuvent transformer votre navigation sur le web. 🌐🛡️ N'hésitez pas à personnaliser cette présentation selon vos besoins et à l'adapter à votre plateforme lab.


## Installation

```
sudo curl -s -S -L https://raw.githubusercontent.com/AdguardTeam/AdGuardHome/master/scripts/install.sh | sh -s -- -v
```

 Ouvrir l’interface web de AdGuard Pour ce faire, utilisez votre navigateur web et ouvrez la page web de AdGuard depuis un navigateur 


ouvrez la page web de AdGuard depuis un navigateur de votre réseau avec l’URL suivante http://IP-DE-ADGUARD:3000/install.html ou http://IP-DE-ADGUARD



![Adguard Home Installation](img/adguard-confiiguration-start.jpg)


